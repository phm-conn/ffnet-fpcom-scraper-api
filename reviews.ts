import type {
  DOMParser,
  Element,
} from "https://deno.land/x/deno_dom@v0.1.22-alpha/deno-dom-native.ts";
import type { CloudflareFetcher } from "./deps.ts";
import { parseReviewPageURL, parseUserURL } from "./regexPaterns.ts";

export interface Review {
  userID: number;
  username: string;
  review: string;
  time: number;
}

export async function getReviews(
  id: number,
  skipLast: number,
  domain: string,
  fetcher: CloudflareFetcher,
  parser: DOMParser,
) {
  let nextPage = 1;
  let lastPage = 0;
  let finalPage = 1;
  const reviews: Review[] = [];
  while (lastPage < nextPage) {
    lastPage++;
    const pageResults = await getReviewPage(
      lastPage,
      id,
      domain,
      fetcher,
      parser,
    );
    if (pageResults.nextPage >= 1) {
      nextPage = pageResults.nextPage;
    }
    if (pageResults.finalPage >= 1) {
      finalPage = pageResults.finalPage;
    }
    if (nextPage > finalPage) {
      finalPage = nextPage;
    }
    reviews.push(...pageResults.reviews);
    if (nextPage !== finalPage) {
      if (finalPage - lastPage <= skipLast) {
        break;
      }
    }
  }
  if (lastPage > finalPage) {
    console.warn("Something is wrong, last page > final page?");
    console.debug({ lastPage, finalPage });
  }
  if (lastPage < finalPage - skipLast) {
    console.warn("Something is wrong, last page < final page?");
    console.debug({ lastPage, finalPage });
  }
  return {
    reviews,
    finalPage,
    loadedPages: lastPage,
  };
}

async function getReviewPage(
  page: number,
  id: number,
  domain: string,
  fetcher: CloudflareFetcher,
  parser: DOMParser,
) {
  const { content } = await fetcher.fetch(
    `https://${domain}/r/${id}/0/${page}/`,
  );
  const dom = parser.parseFromString(content, "text/html");
  if (!dom) {
    throw new Error("DOM Parsing issue.");
  }
  const reviewElems = dom.querySelectorAll("#gui_table1i tbody tr td");
  const reviews: Review[] = [];
  for (const node of reviewElems) {
    const elem = node as Element;
    let user;
    const links = elem.querySelectorAll("a");
    if (links.length == 3) {
      const link = links[2] as Element;
      user = parseUserURL(link.attributes.getNamedItem("href").value);
    }
    if (!user) {
      continue;
    }
    const reviewNode = elem.querySelector("div");
    if (!reviewNode) {
      continue;
    }
    const reviewElem = reviewNode as Element;
    const review: Review = {
      userID: user.id,
      username: user.name,
      review: reviewElem.innerText,
      time: parseInt(
        elem.querySelector("span[data-xutime]")?.attributes.getNamedItem(
          "data-xutime",
        ).value || "-1",
      ),
    };
    reviews.push(review);
  }
  const pages = dom.querySelectorAll("center > a");
  let finalPage = -Infinity;
  let nextPage = -Infinity;
  for (const node of pages) {
    const pageElem = node as Element;
    if (pageElem.innerText == "Last") {
      finalPage =
        parseReviewPageURL(pageElem.attributes.getNamedItem("href").value).page;
    }
    if (pageElem.innerText == "Next »") {
      nextPage =
        parseReviewPageURL(pageElem.attributes.getNamedItem("href").value).page;
    }
  }
  return {
    reviews,
    nextPage,
    finalPage,
  };
}
