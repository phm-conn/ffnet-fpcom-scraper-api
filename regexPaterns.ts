export const userURLParser = /\/u\/(?<id>[0-9]+)\/(?<name>[a-zA-Z]+)/;
export function parseUserURL(url: string) {
  const match = url.match(userURLParser)?.groups;
  return {
    id: match?.id ? parseInt(match.id) : 0,
    name: match?.name ? match.name : "unknown",
  };
}

export const reviewPageURLParser =
  /\/r\/(?<story>[0-9]+)\/0\/(?<page>[0-9]+)\//;

export function parseReviewPageURL(url: string) {
  const match = url.match(reviewPageURLParser)?.groups;
  return {
    story: match?.story ? parseInt(match.story) : -Infinity,
    page: match?.page ? parseInt(match.page) : -Infinity,
  };
}
