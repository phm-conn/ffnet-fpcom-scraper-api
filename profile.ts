import type {
  DOMParser,
  Element,
  HTMLDocument,
} from "https://deno.land/x/deno_dom@v0.1.22-alpha/deno-dom-native.ts";
import {
  NodeList,
} from "https://deno.land/x/deno_dom@v0.1.22-alpha/deno-dom-native.ts";
import type { CloudflareFetcher } from "./deps.ts";
import { parseUserURL } from "./regexPaterns.ts";

const idParser = /id: (?<id>[0-9]+)/;
function parseID(s: string) {
  const match = s.match(idParser)?.groups;
  return match?.id ? parseInt(match.id) : 0;
}

export interface Story {
  id: number;
  title: string;
  category: string;
  wordcount: number;
  datesubmit: number;
  dateupdate: number;
  ratingtimes: number;
  chapters: number;
  statusid: number;
  authorID: number;
  authorName: string;
  blurb: string;
  tags: string[];
}

export async function getProfile(
  id: number,
  domain: string,
  fetcher: CloudflareFetcher,
  parser: DOMParser,
) {
  const { content } = await fetcher.fetch(`https://${domain}/u/${id}/`);
  return parseProfile(content, parser);
}

export async function getProfileByUsername(
  user: string,
  domain: string,
  fetcher: CloudflareFetcher,
  parser: DOMParser,
) {
  const { content } = await fetcher.fetch(`https://${domain}/~${user}`);
  return parseProfile(content, parser);
}

function parseProfile(content: string, parser: DOMParser) {
  const dom = parser.parseFromString(content, "text/html");
  if (!dom) {
    throw new Error("DOM Parsing issue.");
  }
  const name = dom.querySelector("#content_wrapper_inner span")?.textContent ||
    "MissingN0";
  const id = parseID(
    dom.querySelectorAll("table table td")[1].childNodes[2].textContent,
  );
  const favStories = parseStories(".z-list.favstories", dom);
  const myStories = parseStories(".z-list.mystories", dom, id, name);
  return {
    name,
    favStories,
    myStories,
  };
}

function parseStories(
  selector: string,
  dom: HTMLDocument,
  id?: number,
  name?: string,
) {
  const list = dom?.querySelectorAll(selector);
  const parsedStories: Story[] = [];
  for (const item of list) {
    const elem = item as Element;
    const attr = elem.attributes;
    const authorURL = elem.children[1].attributes.getNamedItem("href").value ||
      "";
    let parsedAuthor = parseUserURL(authorURL);
    if (id && name) {
      parsedAuthor = {
        id,
        name,
      };
    }
    const authorID = parsedAuthor.id;
    const authorName = parsedAuthor.name;
    const tags: string[] = (() => {
      const nodes = elem.querySelector(".z-padtop2")?.childNodes;
      let s = nodes?.item(nodes.length - 1).textContent || "";
      s = s.replace("- ", "");
      let t = s.split(",");
      t = t.map((tag) => tag.trim());
      return t;
    })();
    const blurb =
      elem.querySelector(".z-indent.z-padtop")?.childNodes[0].textContent ||
      "MissingN0";
    const story: Story = {
      id: parseInt(attr.getNamedItem("data-storyid").value),
      title: attr.getNamedItem("data-title").value,
      category: attr.getNamedItem("data-category").value,
      wordcount: parseInt(attr.getNamedItem("data-wordcount").value),
      datesubmit: parseInt(attr.getNamedItem("data-datesubmit").value),
      dateupdate: parseInt(attr.getNamedItem("data-dateupdate").value),
      ratingtimes: parseInt(attr.getNamedItem("data-ratingtimes").value),
      chapters: parseInt(attr.getNamedItem("data-chapters").value),
      statusid: parseInt(attr.getNamedItem("data-statusid").value),
      authorID,
      authorName,
      blurb,
      tags,
    };
    parsedStories.push(story);
  }
  return parsedStories;
}

function _parsePeople(list: NodeList) {
  list;
}
