import * as test from "https://deno.land/std@0.133.0/testing/asserts.ts";

import { FictionPressParser } from "./mod.ts";

Deno.test({
  name: "Attempt without setup.",
  async fn(t) {
    const fpp = new FictionPressParser(
      "FictionPressParser-Test-Instance",
      "www.fanfiction.net",
    );
    await t.step("Profile by ID", async () => {
      await test.assertRejects(async () => {
        const __ = await fpp.profile(15379546);
      });
    });
    await t.step("Profile by Username", async () => {
      await test.assertRejects(async () => {
        const __ = await fpp.profileByUsername("phantomconnections");
      });
    });
    await t.step("Review by ID", async () => {
      await test.assertRejects(async () => {
        const __ = await fpp.reviews(14064084);
      });
    });
  },
});

Deno.test({
  name: "Download Phantom-Connections's Profile from FF.Net by ID",
  async fn() {
    const fpp = new FictionPressParser(
      "FictionPressParser-Test-Instance",
      "www.fanfiction.net",
    );
    await fpp.setup();
    const profile = await fpp.profile(15379546);
    test.assertEquals(profile.favStories.length, 1);
    test.assertEquals(profile.favStories[0].authorID, 4463712);
    test.assertEquals(profile.favStories[0].id, 11975691);
    test.assert(
      (profile.favStories[0].title == "Well that\\'s disappointing") ||
        (profile.favStories[0].title == "Well that's disappointing"),
    );
    test.assertEquals(profile.favStories[0].authorID, 4463712);
    await fpp.tearDown();
  },
});
Deno.test({
  name: "Download Phantom-Connections's Profile from FF.Net by username",
  async fn() {
    const fpp = new FictionPressParser(
      "FictionPressParser-Test-Instance",
      "www.fanfiction.net",
    );
    await fpp.setup();
    const profile = await fpp.profileByUsername("phantomconnections");
    test.assertEquals(profile.favStories.length, 1);
    test.assertEquals(profile.favStories[0].authorID, 4463712);
    test.assertEquals(profile.favStories[0].id, 11975691);
    test.assert(
      (profile.favStories[0].title == "Well that\\'s disappointing") ||
        (profile.favStories[0].title == "Well that's disappointing"),
    );
    test.assertEquals(profile.favStories[0].authorID, 4463712);
    await fpp.tearDown();
  },
});
Deno.test({
  name: "Download WWJDTD's Profile from FF.Net by ID",
  async fn() {
    const fpp = new FictionPressParser(
      "FictionPressParser-Test-Instance",
      "www.fanfiction.net",
    );
    await fpp.setup();
    const profile = await fpp.profile(4463712);
    // Up to date as of 07.04.22
    test.assert(profile.favStories.length >= 130);
    await fpp.tearDown();
  },
});

Deno.test({
  name: "Download 14064084 from FF.Net Full",
  async fn(t) {
    const fpp = new FictionPressParser(
      "FictionPressParser-Test-Instance",
      "www.fanfiction.net",
    );
    await fpp.setup();
    const { reviews } = await fpp.reviews(14064084);
    await t.step("verify there is still only 1 review.", () => {
      test.assertEquals(reviews.length, 1);
    });
    test.assertStringIncludes(reviews[0].review, "314");
    test.assertStringIncludes(reviews[0].review, "235711");
    test.assertEquals(reviews[0].userID, 4463712);
    await fpp.tearDown();
  },
});
Deno.test({
  name: "Download 13035069 from FF.Net Full Partial",
  async fn(t) {
    const fpp = new FictionPressParser(
      "FictionPressParser-Test-Instance",
      "www.fanfiction.net",
    );
    await fpp.setup();
    let totalPages = 0;
    let firstUser = 0;
    await t.step("Full Story Count", async () => {
      const { reviews, finalPage, loadedPages } = await fpp.reviews(13035069);
      // Review count as of 07.04.2022
      test.assert(reviews.length >= 122);
      test.assertEquals(finalPage, 16);
      test.assertEquals(finalPage, loadedPages);
      firstUser = reviews[0].userID;
      totalPages = finalPage;
    });
    test.assertNotEquals(totalPages, 0);
    await t.step("Skipping just under half the pages", async () => {
      const skip = Math.floor(totalPages / 2);
      const { reviews, finalPage, loadedPages } = await fpp.reviews(
        13035069,
        skip,
      );
      // Review count as of 07.04.2022
      test.assertEquals(finalPage, totalPages);
      test.assertEquals(loadedPages, finalPage - skip);
      test.assertEquals(reviews[0].userID, firstUser);
    });
    await fpp.tearDown();
  },
});
