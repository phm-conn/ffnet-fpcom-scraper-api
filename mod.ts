import {
  DOMParser,
} from "https://deno.land/x/deno_dom@v0.1.22-alpha/deno-dom-native.ts";
import { CloudflareFetcher } from "./deps.ts";
import { getReviews } from "./reviews.ts";
import { getProfile, getProfileByUsername } from "./profile.ts";

export class FictionPressParser {
  private fetcher: CloudflareFetcher;
  private parser: DOMParser;
  private domain: "www.fanfiction.net" | "www.fictionpress.com";
  private pitched = false;

  constructor(
    useragent: string,
    domain: "www.fanfiction.net" | "www.fictionpress.com",
    timeout?: number,
  ) {
    this.fetcher = new CloudflareFetcher(useragent, timeout);
    this.parser = new DOMParser();
    this.domain = domain;
  }

  async setup() {
    await this.fetcher.startBrowser();
    this.pitched = true;
  }

  async tearDown() {
    this.pitched = false;
    await this.fetcher.closeBrowser();
  }

  async profile(id: number) {
    if (!this.pitched) {
      throw new Error("Setup the fetcher first!");
    }
    return await getProfile(id, this.domain, this.fetcher, this.parser);
  }

  async profileByUsername(username: string) {
    if (!this.pitched) {
      throw new Error("Setup the fetcher first!");
    }
    return await getProfileByUsername(
      username,
      this.domain,
      this.fetcher,
      this.parser,
    );
  }

  async reviews(id: number, skipLastPages = 0) {
    if (!this.pitched) {
      throw new Error("Setup the fetcher first!");
    }
    return await getReviews(
      id,
      skipLastPages,
      this.domain,
      this.fetcher,
      this.parser,
    );
  }
}
